﻿using Drzn.Test.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Drzn.Test.Areas.Car.Models
{
    public class CarBrand : Entity
    {
        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }
    }
}