﻿using Drzn.Test.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Drzn.Test.Areas.Car.Models
{
    public class CarPart : Entity
    {
        [Required]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Марка")]
        public virtual CarBrand Brand { get; set; }

        public Guid BrandId { get; set; }

        [Display(Name = "Название")]
        public string FullTitle
        {
            get
            {
                if (this.Brand != null)
                    return string.Format("{0} ({1})", this.Title, this.Brand.Title);

                return this.Title;
            }
        }
    }
}