﻿using Drzn.Test.Areas.Car.Models;
using Drzn.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Drzn.Test.Areas.Car.Controllers
{
    public class CarPartsController : Controller
    {
        public ActionResult Index()
        {
            var context = DataContext.JoinOrCreate();
            var parts = context.CarParts.ToList();

            return View(parts);
        }

        public ActionResult Search(string term)
        {
            var context = DataContext.JoinOrCreate();

            var parts = context.CarParts.Where(x => x.Title.Contains(term) || x.Brand.Title.Contains(term))
                .ToList()
                .Select(x => x.FullTitle);

            return Json(parts, JsonRequestBehavior.AllowGet);
        }
    }
}