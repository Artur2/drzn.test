﻿using Drzn.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Drzn.Test.Areas.Car.Controllers
{
    public class CarBrandsController : Controller
    {
        // GET: Car/CarBrands
        public ActionResult Index()
        {
            var context = DataContext.JoinOrCreate();

            var brands = context.CarBrands.ToList();

            return View(brands);
        }
    }
}