﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Drzn.Test.Models
{
    [DebuggerDisplay("Id={Id}")]
    public class Entity
    {
        [Key]
        [Display(Name = "ИД")]
        public Guid Id { get; set; }
    }
}