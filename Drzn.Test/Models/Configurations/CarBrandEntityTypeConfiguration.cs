﻿using Drzn.Test.Areas.Car.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Drzn.Test.Models.Configurations
{
    public class CarBrandEntityTypeConfiguration : EntityTypeConfiguration<CarBrand>
    {
        public CarBrandEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(256);
        }
    }
}