﻿using Drzn.Test.Areas.Car.Models;
using Drzn.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace Drzn.Test.Models.Configurations
{
    public class MigrateToLatestVersionConfiguration : DbMigrationsConfiguration<DataContext>
    {
        public MigrateToLatestVersionConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(DataContext context)
        {
            context.CarBrands.AddOrUpdate(x => x.Title,
                new CarBrand { Id = Guid.NewGuid(), Title = "BMW" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Honda" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Mercedes-Benz" },
                new CarBrand { Id = Guid.NewGuid(), Title = "AC" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Acura" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Alfa Romeo" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Aro" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Asia" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Aston Martin" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Audi" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Austin" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Bentley" },
                new CarBrand { Id = Guid.NewGuid(), Title = "BMW Alpina" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Brilliance" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Bugatti" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Buick" },
                new CarBrand { Id = Guid.NewGuid(), Title = "BYD" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Cadillac" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Caterham" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Changan" },
                new CarBrand { Id = Guid.NewGuid(), Title = "ChangFeng" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Chery" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Chevrolet" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Chrysler" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Citroen" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Dacia" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Daewoo" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Daihatsu" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Daimler" },
                new CarBrand { Id = Guid.NewGuid(), Title = "De Lorean" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Derways" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Dodge" },
                new CarBrand { Id = Guid.NewGuid(), Title = "DongFeng" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Doninvest" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Eagle" },
                new CarBrand { Id = Guid.NewGuid(), Title = "FAW" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Ferrari" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Fiat" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Fisker" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Ford" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Foton" },
                new CarBrand { Id = Guid.NewGuid(), Title = "Geely" },
                new CarBrand { Id = Guid.NewGuid(), Title = "GMC" }
                );

            var bmw = context.CarBrands.Local.FirstOrDefault(x => x.Title == "BMW");

            context.CarParts.AddOrUpdate(x => x.Title,
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW 5w30 High Performance Synthetic Oil" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW E71 X6 w/ N54/N55 Oil Change Kit" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW N54 Updated Fuel Injector Sets" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW Antifreeze/Coolant" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "OEM BMW Carpeted Floor Mats with BMW Lettering on Heel Pad" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "BMW OEM E71 X6 All Weather Rubber Floor Liners" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "OEM BMW All Weather Rubber Floor Mats" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "OEM BMW All Weather Cargo Liner" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW Performance E71 X6 Glossy Black Kidney Grills" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW E71 X6 Black Line Tail Light Kit" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW E71 X6 Y Spoke Style 336 Rims" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW RDC (TPMS) Module/Valve Set" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "Genuine BMW New Style Center Caps with Chrome Ring (Set of 4)" },
                new CarPart { Id = Guid.NewGuid(), Brand = bmw, BrandId = bmw.Id, Title = "OEM BMW E70/71 X5 & X6 Series Emergency Wheel/Tire Set" }
                );

            var mercedes = context.CarBrands.Local.FirstOrDefault(x => x.Title == "Mercedes-Benz");

            context.CarParts.AddOrUpdate(x => x.Title,
                new CarPart { Id = Guid.NewGuid(), Brand = mercedes, BrandId = mercedes.Id, Title = "MB12228PP1 Cabin Air filter" },
                new CarPart { Id = Guid.NewGuid(), Brand = mercedes, BrandId = mercedes.Id, Title = "ALARM SIREN" },
                new CarPart { Id = Guid.NewGuid(), Brand = mercedes, BrandId = mercedes.Id, Title = "ANTENNA COMBINER. ANTENNA SPLITTER. . Replaced By 67823111" },
                new CarPart { Id = Guid.NewGuid(), Brand = mercedes, BrandId = mercedes.Id, Title = "BATTERY CUT-OFF RELAY. RELAY." },
                new CarPart { Id = Guid.NewGuid(), Brand = mercedes, BrandId = mercedes.Id, Title = "BATTERY." },
                new CarPart { Id = Guid.NewGuid(), Brand = mercedes, BrandId = mercedes.Id, Title = "BLIND PLUG. DUMMY PLUG. GASKET." }
                );

            context.Database.ExecuteSqlCommand(@"
                IF NOT EXISTS(SELECT * FROM sys.indexes i where i.name = 'CarParts_Title_IX') 
                CREATE NONCLUSTERED INDEX CarParts_Title_IX ON dbo.CarParts (Title)");
            context.Database.ExecuteSqlCommand(@"
                IF NOT EXISTS(SELECT * FROM sys.indexes i where i.name = 'CarBrands_Title_IX')
                CREATE NONCLUSTERED INDEX CarBrands_Title_IX ON dbo.CarBrands (Title)"
                );

            base.Seed(context);
        }
    }
}