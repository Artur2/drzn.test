﻿using Drzn.Test.Areas.Car.Models;
using Drzn.Test.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

namespace Drzn.Test.Models.UnitOfWork
{
    public class DataContext : DbContext
    {
        [ThreadStatic]
        private static DataContext context;

        protected int? CreatedThreadId { get; set; }

        public DataContext()
            : base("DefaultConnection")
        {
#if DEBUG
            Database.Log = s => Debug.WriteLine(s);
#endif
        }


        public static DataContext JoinOrCreate()
        {
            if (context == null)
                context = new DataContext
                {
                    CreatedThreadId = Thread.CurrentThread.ManagedThreadId
                };

            return context;
        }

        private void EnsureThreadSafety()
        {
            if (CreatedThreadId.HasValue && !CreatedThreadId.Value.Equals(Thread.CurrentThread.ManagedThreadId))
                throw new InvalidOperationException("DbContext is not thread safe, use it only in single thread");
        }

        public static void Close()
        {
            if (context != null)
            {
                context.EnsureThreadSafety();
                context = null;
            }
        }

        private IDbSet<CarPart> carParts;

        public IDbSet<CarPart> CarParts
        {
            get
            {
                EnsureThreadSafety();
                if (carParts == null)
                    carParts = Set<CarPart>();

                return carParts;
            }
        }

        private IDbSet<CarBrand> carBrands;

        public IDbSet<CarBrand> CarBrands
        {
            get
            {
                EnsureThreadSafety();
                if (carBrands == null)
                    carBrands = Set<CarBrand>();

                return carBrands;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CarPartEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new CarBrandEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}